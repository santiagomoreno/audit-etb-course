Staking.sol

_stake() linea 73
should be _addressToStakedAmount[msg.sender] += msg.value;

_stake() linea 76
should be !_addressToIsValidator[msg.sender] in the if.
There is a DoS attack because it can never be executed inside the if when 
staking and the require function of the _unstake() function will always throws an exception.


 _unstake() linea 100
 Dont follow check-effect-interaction pattern

 
 unstake() linea 66
a smart contract can executed unstake()
could be external

_unstake() line 89 
the require could have an easier to understand message
"Number of validators can't be less than MinimumRequiredNumValidators"

 onlyEOA modifier dont use it.

Is a honeypot, steps:

- people deposit ether via receive() or stake()
- Those people will not be set as validators
- The owner sets a smart contract as a validator (addValidator)
- That smart contract stakes ether
- Then it executes unstake and on the attacker's receive it has a call to unstake() again.

In this case, a honeypot is mixed with a reentrancy attack.