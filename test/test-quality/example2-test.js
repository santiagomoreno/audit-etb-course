const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("example-2", function () {
    let deployer, user, user_2;

    beforeEach(async function () {
        [deployer, user, user_2] = await ethers.getSigners();
        const Example2 = await ethers.getContractFactory("Example2", deployer);
        this.example2 = await Example2.deploy();
    });

    describe("Example", function () {
        it("set param with positive number", async function () {
            await this.example2.change(5);
            expect(5).to.eq(await this.example2.param());
        });

        //it("set param with negative number", async function () {
        //    await this.example2.change(-5);
        //    expect(5).to.eq(await this.example2.param());
        //});
    });

});
