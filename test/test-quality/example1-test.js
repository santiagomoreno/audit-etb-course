const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("example-1", function () {

    beforeEach(async function () {
        const Example1 = await ethers.getContractFactory("Example1", deployer);
        this.example1 = await Example1.deploy();
    });

    describe("Example", function () {
        it("set param with positive number", async function () {
            await this.example1.change(5);
            expect(5).to.eq(await this.example1.param());
        });

        it("set param with negative number", async function () {
            await this.example1.change(-5);
            expect(5).to.eq(await this.example1.param());
        });
    });
});
