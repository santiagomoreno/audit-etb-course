const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Pool", function () {
    let owner, user, user_2, user_3, user_4;

    beforeEach(async function () {
        [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

        const ERC20 = await ethers.getContractFactory("ERC20Mock", owner);
        this.stablecoin = await ERC20.deploy("stablecoin", "DAI");

        const CPERC1155 = await ethers.getContractFactory("CPERC1155", owner);
        this.cPERC1155 = await CPERC1155.deploy(["QmS6WUYi2dxyMDkZ5G9yF6YiXbwpZbZV3u5T8Z2LG5bB5V", "QmS6WUYi2dxyMDkZ5G9yF6YiXbwpZbZV3u5T8Z2LG5bB5V"]);

        const OracleMock = await ethers.getContractFactory("OracleMock", owner);
        this.oracleMock = await OracleMock.deploy();

        const Pool = await ethers.getContractFactory("Pool", owner);
        this.pool = await Pool.deploy(this.stablecoin.address, this.nftContract.address, this.oracleMock.address);

        await this.stablecoin.mint(1000);
    });

    describe("buyNFT() Function", function () {
        it("should revert when try to buy nft with 0 DAI", async function () {
            await expect(this.pool.connect(user).buyRandomNFT()).to.be.reverted;
        });

        it("should revert when try to buy nft with 50 DAI without whitelist", async function () {
            await this.stablecoin.transfer(user.address, 50);

            await this.stablecoin.connect(user).increaseAllowance(this.pool.address, 50);

            await this.pool.connect(user).buyRandomNFT();

            expect(50).to.eq(await this.pool.totalPrize());
        });
    });
});

